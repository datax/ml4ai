import { NgModule } from "@angular/core";
import { IonicModule, IonicPageModule } from "ionic-angular";
import { BrowserModule } from "@angular/platform-browser";
import { EnumPage } from "./enum/enum";
import { MenuPage } from "./menu/menu";
import { MeListPage } from "./me-list/me-list";
import { PublishListPage } from "./publish-list/publish-list";
import { InfoListPage } from "./info-list/info-list";
import { TaskListPage } from "./task-list/task-list";
import { HomePage } from "./home/home";
import { LoginPage } from "./login/login";
import { IntraCityDistributionPage } from "./intra-city-distribution/intra-city-distribution";
import { BaiduPage } from "./baidu/baidu";

@NgModule({
    declarations: [
        EnumPage,
        MenuPage,
        MeListPage,
        PublishListPage,
        InfoListPage,
        TaskListPage,
        BaiduPage,
        HomePage,
        LoginPage,
        IntraCityDistributionPage
    ],
    imports: [
        IonicModule,
        IonicPageModule,
        BrowserModule
    ],
    entryComponents: [
        EnumPage,
        MenuPage,
        MeListPage,
        PublishListPage,
        InfoListPage,
        TaskListPage,
        HomePage,
        LoginPage,
        BaiduPage,
        IntraCityDistributionPage
    ],
    exports: [
        EnumPage,
        MenuPage,
        MeListPage,
        PublishListPage,
        InfoListPage,
        TaskListPage,
        HomePage,
        LoginPage,
        BaiduPage,
        IntraCityDistributionPage
    ]
})
export class PagesModule { }