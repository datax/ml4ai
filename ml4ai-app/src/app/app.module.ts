import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';

import { IonicApp, IonicErrorHandler, IonicModule, AlertController, PopoverController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from "@angular/http";
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { ExtendsApp } from './app.component';
import { ComponentsModule } from '../components/components.module';
import { StorageProvider } from '../providers/storage/storage';
import { NetProvider } from '../providers/net/net';
import { GlobalsProvider } from '../providers/globals/globals';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '../directives/directives.module';
import { PagesModule } from '../pages/pages.module';
import { LocationProvider } from '../providers/location/location';

@NgModule({
  declarations: [
    ExtendsApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(ExtendsApp),
    ComponentsModule,
    HttpModule,
    FormsModule,
    DirectivesModule,
    PagesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ExtendsApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    PopoverController,
    AlertController,
    Geolocation,
    GlobalsProvider,
    NetProvider,
    StorageProvider,
    LocationProvider
  ]
})
export class AppModule { }
