import React from 'react';
import ReactMarkdown from 'react-markdown';
import NetProvider from '../../net/net';
import { notification } from 'antd';

const Component = React.Component;

export default class Markdown extends Component {

    constructor(props) {
        super(props);
        this.state = {
            source: ""
        };
        this.net = new NetProvider();
    }

    componentDidMount() {
        this.init(this.props.url);
    }

    componentWillReceiveProps(props) {
        this.init(props.url);
    }

    init(url) {
        let me = this;
        let net = this.net;
        net.get(url, (text) => {
            me.setState({
                source: text
            });
        }, () => {
            notification.open({
                message: '错误',
                description: '你无法访问，可能是页面不存在.',
            });
        });
    }

    render() {
        return (<div>
            <ReactMarkdown escapeHtml={false} skipHtml={false} unwrapDisallowed={false} source={this.state.source}></ReactMarkdown>
        </div>);
    }

}