import $ from 'jquery';

export default class NetProvider {

    postJson(url, data, success, error, context) {
        let me = this;
        $.ajax({
            url: url,
            type: 'post',
            headers: {
                'Content-type': "application/json;charset=utf-8"
            },
            context: context ? context : me,
            cache: false,
            data: JSON.stringify(data),
            success: (response, statusCode, xhr) => {
                if (success) {
                    success(response, statusCode, xhr);
                }
            },
            error: (xhr, statusCode, e) => {
                if (error) {
                    error(xhr, statusCode, e);
                }
            }
        });
    }

    get(url, success, error, context) {
        let me = this;
        $.ajax({
            url: url,
            type: 'get',
            headers: {
            },
            context: context ? context : me,
            cache: false,
            success: (response, statusCode, xhr) => {
                if (success) {
                    success(response, statusCode, xhr);
                }
            },
            error: (xhr, statusCode, e) => {
                if (error) {
                    error(xhr, statusCode, e);
                }
            }
        });
    }

}