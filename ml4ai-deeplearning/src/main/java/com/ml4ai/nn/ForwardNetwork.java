package com.ml4ai.nn;

import com.ml4ai.nn.core.Variable;
import org.apache.commons.lang.NotImplementedException;

public interface ForwardNetwork {

    // 获取学习参数
    public default Variable[] getParameters() {
        throw new UnsupportedOperationException("该操作未支持");
    }

    //前向传播
    public default Variable[] forward(Variable... inputs) {
        throw new UnsupportedOperationException("该方法没有实现");
    }

}
