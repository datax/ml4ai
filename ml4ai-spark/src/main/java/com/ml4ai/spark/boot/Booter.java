package com.ml4ai.spark.boot;

import lombok.SneakyThrows;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;

import java.util.Properties;

/**
 * Created by uesr on 2018/9/12.
 */
public class Booter {


    @SneakyThrows
    public static void main(String[] args) {
        SparkConf conf = new SparkConf();
        conf.setMaster("local[*]");
        conf.setAppName("ml4ai");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SparkSession ss = SparkSession.builder().sparkContext(sc.sc()).getOrCreate();
        SQLContext sql = ss.sqlContext();
        Properties properties = new Properties();
        properties.setProperty("driver", "com.mysql.jdbc.Driver");
        properties.setProperty("user", "root");
        properties.setProperty("password", "root");
        Dataset<Row> dataset = sql.read().parquet("a");//sql.read().jdbc("jdbc:mysql://127.0.0.1:3306/ml4ai?serverTimezone=GMT%2B8", "T_TASK", properties);
        dataset.show();
        sql.registerDataFrameAsTable(dataset, "T");
        sql.sql("insert into T values(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19)");
        sql.sql("select * from T order by data_create_time").show();
    }

}