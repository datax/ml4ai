package com.ml4ai.backend.web.sys;

import com.ml4ai.backend.dto.TicketDTO;
import com.ml4ai.backend.dto.UserDTO;
import com.ml4ai.backend.security.SecurityUtils;
import com.ml4ai.backend.services.TicketService;
import com.ml4ai.backend.utils.RestUtil;
import com.ml4ai.backend.utils.StringHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by uesr on 2018/9/16.
 */
@RestController
@RequestMapping("/api/sys/user")
public class SystemRest {

    @Autowired
    TicketService ticketService;

    @RequestMapping("/ticket")
    public Map<String, Object> ticket() {
        UserDTO user = SecurityUtils.getCurrentUser();
        if (user == null) {
            return RestUtil.failure();
        } else {
            TicketDTO ticket = new TicketDTO();
            ticket.setStatus("1");
            ticket.setTicket(StringHelper.uuid());
            ticket.setUser(user);
            ticket = ticketService.save(ticket);
            return RestUtil.success(ticket.getTicket());
        }
    }

}
