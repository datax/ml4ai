package com.ml4ai.backend.web.util;

import com.ml4ai.backend.consts.Const;
import com.ml4ai.backend.dto.IntraCityDistributionTaskDTO;
import com.ml4ai.backend.dto.SiteDTO;
import com.ml4ai.backend.dto.TaskDTO;
import com.ml4ai.backend.dto.UserDTO;
import com.ml4ai.backend.utils.Bean2Map;
import com.ml4ai.backend.utils.PropertyMapper;

import java.util.Map;

/**
 * Created by uesr on 2018/9/9.
 */
public class MapSpec {

    public static Map<String, Object> user2map(UserDTO user) {
        Map<String, Object> map = new Bean2Map().addIgnores(Const.getNotCopyAuditFields()).map(user);
        return map;
    }

    public static Map<String, Object> task2map(TaskDTO task) {
        Map<String, Object> map = new Bean2Map().addIgnores(Const.getNotCopyAuditFields()).addPropMapper(
                new PropertyMapper<>("starter", MapSpec::user2map),
                new PropertyMapper<>("taker", MapSpec::user2map),
                new PropertyMapper<>("site", MapSpec::site2map)
        ).map(task);
        return map;
    }

    public static Map<String, Object> intraCityDistributionTask2map(IntraCityDistributionTaskDTO intraCityDistributionTask) {
        Map<String, Object> map = new Bean2Map().addIgnores(Const.getNotCopyAuditFields()).addPropMapper(
                new PropertyMapper<>("starter", MapSpec::user2map),
                new PropertyMapper<>("taker", MapSpec::user2map),
                new PropertyMapper<>("site", MapSpec::site2map)
        ).map(intraCityDistributionTask);
        return map;
    }

    public static Map<String, Object> site2map(SiteDTO site) {
        Map<String, Object> map = new Bean2Map().addIgnores(Const.getNotCopyAuditFields()).addPropMapper(
                new PropertyMapper<>("parent", MapSpec::site2map)
        ).map(site);
        return map;
    }

}
