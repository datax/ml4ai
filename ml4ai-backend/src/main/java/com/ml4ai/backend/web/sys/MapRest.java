package com.ml4ai.backend.web.sys;

import com.ml4ai.backend.dto.MapAddressDTO;
import com.ml4ai.backend.dto.MapRouteMatrixDTO;
import com.ml4ai.backend.dto.TuplePointDTO;
import com.ml4ai.backend.services.MapService;
import com.ml4ai.backend.utils.RestUtil;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by leecheng on 2018/9/24.
 */
@RestController
@RequestMapping("/api/map")
public class MapRest {

    @Autowired
    MapService mapService;

    @RequestMapping("/getDistance")
    public Map<String, Object> getDistance(@RequestBody TuplePointDTO t) {
        MapRouteMatrixDTO mapRouteMatrixDTO = new MapRouteMatrixDTO();
        mapRouteMatrixDTO.setOrigins(Arrays.asList(new MapAddressDTO(null, t.getFirst().getX(), t.getFirst().getY())));
        mapRouteMatrixDTO.setDestinations(Arrays.asList(new MapAddressDTO(null, t.getSecond().getX(), t.getSecond().getY())));
        mapRouteMatrixDTO.setOperator(MapRouteMatrixDTO.OperatorRiding);
        mapRouteMatrixDTO.setTactics(10);
        Map<String, Object> route = mapService.route(mapRouteMatrixDTO);
        if (route != null) {
            List<Map<String, Object>> result = (List) route.get("result");
            Map<String, Object> first = result.get(0);
            String distance = (String) first.get("distance");
            return RestUtil.success(distance);
        } else {
            return RestUtil.failure("获取信息失败!");
        }
    }

}
