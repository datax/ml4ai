package com.ml4ai.backend.web.sys;

import com.ml4ai.backend.dto.MenuDTO;
import com.ml4ai.backend.services.MenuService;
import com.ml4ai.backend.utils.RestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created by uesr on 2018/9/4.
 */
@CrossOrigin
@RestController
@RequestMapping("/api/menu")
public class MenuRest {

    @Autowired
    MenuService menuService;

    @RequestMapping("/get")
    public Map<String, Object> get() {
        return RestUtil.success(menuService.getAllMenus());
    }

    @RequestMapping("/query")
    public Map<String,Object> query(@RequestBody MenuDTO menuDTO){
        menuDTO.setStatus("1");
        List<MenuDTO> sub = menuService.query(menuDTO);
        return RestUtil.success(sub);
    }

    @RequestMapping("/save")
    public Map<String,Object> save(@RequestBody MenuDTO menu){
        menu.setStatus("1");
        MenuDTO save = menuService.save(menu);
        return RestUtil.success(save);
    }

    @RequestMapping("/delete")
    public Map<String,Object> delete(@RequestBody MenuDTO menu){
        return RestUtil.success( menuService.delete(menu.getId()));
    }
}
