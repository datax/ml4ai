package com.ml4ai.backend.services;

import com.ml4ai.backend.domain.Role;
import com.ml4ai.backend.dto.RoleDTO;
import com.ml4ai.backend.services.base.BaseService;

/**
 * Created by leecheng on 2018/9/24.
 */
public interface RoleService extends BaseService<Role, RoleDTO> {
}
