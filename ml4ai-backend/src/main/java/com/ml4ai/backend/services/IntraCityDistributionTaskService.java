package com.ml4ai.backend.services;

import com.ml4ai.backend.domain.IntraCityDistributionTask;
import com.ml4ai.backend.dto.IntraCityDistributionTaskDTO;
import com.ml4ai.backend.services.base.BaseService;

/**
 * Created by uesr on 2018/9/12.
 */
public interface IntraCityDistributionTaskService extends BaseService<IntraCityDistributionTask, IntraCityDistributionTaskDTO> {
}
