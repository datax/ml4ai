package com.ml4ai.backend.services;

import com.ml4ai.backend.domain.Ticket;
import com.ml4ai.backend.dto.TicketDTO;
import com.ml4ai.backend.services.base.BaseService;

/**
 * Created by uesr on 2018/9/16.
 */
public interface TicketService extends BaseService<Ticket, TicketDTO> {
}
