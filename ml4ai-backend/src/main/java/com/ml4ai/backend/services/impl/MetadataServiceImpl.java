package com.ml4ai.backend.services.impl;

import com.ml4ai.backend.domain.Metadata;
import com.ml4ai.backend.repository.MetadataRepository;
import com.ml4ai.backend.services.MetadataService;
import com.ml4ai.backend.utils.Bean2Bean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by uesr on 2018/9/8.
 */
@Service
@Transactional
public class MetadataServiceImpl implements MetadataService {

    @Autowired
    MetadataRepository metadataRepo;

    @Override
    public MetadataInstance attach(String key) {
        Metadata metadata = metadataRepo.findByKey(key);
        if (metadata != null) {
            Metadata newMetadata = new Metadata();
            new Bean2Bean().copyProperties(metadata, newMetadata);
            return new MetadataInstanceImpl(newMetadata);
        } else {
            throw new IllegalStateException("找不到Metadata [" + key + "]");
        }
    }


    public static class MetadataInstanceImpl implements MetadataInstance {

        private Metadata metadata;

        public MetadataInstanceImpl(Metadata metadata) {
            this.metadata = metadata;
        }

        @Override
        public int put(String rowKey, Map<String, String> data) {
            return 0;
        }

        @Override
        public Map<String, String> get(String rowKey) {
            return null;
        }

        @Override
        public List<Map<String, String>> scan(Function<Map<String, String>, Boolean> checker) {
            return null;
        }
    }

}
