package com.ml4ai.backend.services.impl;

import com.ml4ai.backend.dto.MenuDTO;
import com.ml4ai.backend.dto.UserDTO;
import com.ml4ai.backend.services.MenuService;
import com.ml4ai.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by leecheng on 2018/9/24.
 */
@Service
@Transactional
public class InitServiceImpl implements CommandLineRunner {

    @Autowired
    UserService userService;

    @Autowired
    MenuService menuService;

    @Override
    public void run(String... strings) throws Exception {
        UserDTO queryUser = new UserDTO();
        queryUser.setStatus("1");
        Long count = userService.count(queryUser);
        if (count < 1L) {
            UserDTO user = new UserDTO();
            user.setStatus("1");
            user.setLogin("admin");
            user.setPassword("admin");
            user.setNick("管理员");
            user.setTelephone("");
            userService.save(user);
        }

        MenuDTO queryMenu = new MenuDTO();
        queryMenu.setStatus("1");
        Long countMenu = menuService.count(queryMenu);
        if (countMenu < 1L) {
            MenuDTO root = new MenuDTO();
            root.setStatus("1");
            root.setMenuName("系统管理");
            root = menuService.save(root);

            MenuDTO menu = new MenuDTO();
            menu.setStatus("1");
            menu.setMenuName("菜单");
            menu.setMenuData("MenuComponent");
            menu.setParent(root);
            menuService.save(menu);
        }

    }
}
