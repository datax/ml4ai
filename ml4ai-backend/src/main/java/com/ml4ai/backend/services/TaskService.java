package com.ml4ai.backend.services;

import com.ml4ai.backend.domain.Task;
import com.ml4ai.backend.domain.base.BusinessType;
import com.ml4ai.backend.dto.TaskDTO;
import com.ml4ai.backend.services.base.BaseService;

/**
 * Created by uesr on 2018/9/9.
 */
public interface TaskService extends BaseService<Task, TaskDTO> {

    String genCode(BusinessType businessType);

}
