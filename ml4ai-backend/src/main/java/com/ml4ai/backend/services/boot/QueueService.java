package com.ml4ai.backend.services.boot;

/**
 * Created by uesr on 2018/9/16.
 */
public interface QueueService {

    void auto();

    void onMessageArrived(Object msg);
}
