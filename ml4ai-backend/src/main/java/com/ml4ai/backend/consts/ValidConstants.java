package com.ml4ai.backend.consts;

/**
 * Created by leecheng on 2017/7/7.
 */
public class ValidConstants {

    private ValidConstants(){
        throw new IllegalStateException("常量宿主");
    }

    public static final String NOT_NULL = "notNull";
    public static final String NULL = "null";
    public static final String NUMBER = "number";
    public static final String REGEX = "regex";
    public static final String DEFAULT_TIME = "defaultTime";
    public static final String DEFAULT_DATE = "defaultDate";

}
