package com.ml4ai.backend.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class JSONUtil {

    private static Gson defaultGson = new Gson();

    public static String toJson(Object object) {
        return defaultGson.toJson(object);
    }

    public static <T> T toObject(String json, Class<T> tc) {
        return defaultGson.fromJson(json, tc);
    }

    public static JsonObject toJsonObject(String src) {
        return defaultGson.fromJson(src, JsonObject.class);
    }
}
