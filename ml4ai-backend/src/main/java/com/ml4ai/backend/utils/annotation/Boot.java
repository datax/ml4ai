package com.ml4ai.backend.utils.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by leecheng on 2017/9/27.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Boot {
}
