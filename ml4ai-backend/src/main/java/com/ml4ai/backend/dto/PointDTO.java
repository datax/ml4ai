package com.ml4ai.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.*;

/**
 * Created by leecheng on 2018/9/24.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PointDTO {

    private Double x;
    private Double y;
    private String address;

    public PointDTO(Double x, Double y) {
        this(x, y, null);
    }

    public PointDTO(String address) {
        this(null, null, address);
    }

}
