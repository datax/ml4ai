package com.ml4ai.backend.dto;

import com.ml4ai.backend.dto.base.BaseAuditDTO;
import com.ml4ai.backend.utils.annotation.QueryColumn;
import lombok.Data;

/**
 * Created by leecheng on 2018/9/24.
 */
@Data
public class RoleDTO extends BaseAuditDTO {

    private String roleName;

    private String roleCode;

    private String roleData;

    @QueryColumn(propName = "users.id")
    private Long userId;
}
