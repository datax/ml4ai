package com.ml4ai.backend.dto.base;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by uesr on 2018/9/2.
 */
@Getter
@Setter
public class BaseDTO implements Serializable {

    private Long id;

}
