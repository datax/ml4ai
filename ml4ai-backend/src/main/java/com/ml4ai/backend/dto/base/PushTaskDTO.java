package com.ml4ai.backend.dto.base;

import com.ml4ai.backend.type.PushType;
import lombok.Data;

import java.util.List;

/**
 * Created by uesr on 2018/9/16.
 */
@Data
public class PushTaskDTO {

    private PushType pushType;
    private List<Long> pushUsers;
    private PushBaseDTO data;

}
