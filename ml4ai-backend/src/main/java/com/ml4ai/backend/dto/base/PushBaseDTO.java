package com.ml4ai.backend.dto.base;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by uesr on 2018/9/16.
 */
@Data
public class PushBaseDTO implements Serializable {

    private String code;
    private String msg;
    private Object data;

}
