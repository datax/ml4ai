package com.ml4ai.backend.dto;

import com.ml4ai.backend.domain.User;
import com.ml4ai.backend.dto.base.BaseAuditDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by uesr on 2018/9/16.
 */
@Getter
@Setter
public class TicketDTO extends BaseAuditDTO {

    private String ticket;

    private Long userId;
    private UserDTO user;

}
