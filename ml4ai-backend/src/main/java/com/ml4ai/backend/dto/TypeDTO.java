package com.ml4ai.backend.dto;

import com.ml4ai.backend.dto.base.BaseAuditDTO;
import lombok.Data;

/**
 * Created by uesr on 2018/9/12.
 */
@Data
public class TypeDTO extends BaseAuditDTO {

    private String typeName;

    private String typeCode;

    private Long parentId;

    private TypeDTO parent;

}
