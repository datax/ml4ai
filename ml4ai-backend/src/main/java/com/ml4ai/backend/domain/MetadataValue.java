package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/8.
 */
@Setter
@Getter
@Entity
@Table(name = "T_METADATA_VALUE")
public class MetadataValue extends BaseAuditEntity {

    @ManyToOne
    @JoinColumn
    private MetadataColumn metadataColumn;

    @ManyToOne
    @JoinColumn
    private MetadataRow metadataRow;

    @Lob
    @Column(name = "c_value")
    private String value;
}
