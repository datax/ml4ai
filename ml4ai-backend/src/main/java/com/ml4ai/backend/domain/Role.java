package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by uesr on 2018/9/9.
 */
@Entity
@Table(name = "T_ROLE")
@Getter
@Setter
public class Role extends BaseAuditEntity {

    @Column
    private String roleName;

    @Column
    private String roleCode;

    @Lob
    @Column
    private String roleData;

    @ManyToMany
    @JoinTable(name = "T_USER_ROLE", joinColumns = {@JoinColumn(name = "c_role")}, inverseJoinColumns = {@JoinColumn(name = "c_user")})
    private List<User> users;

    @ManyToMany
    @JoinTable(name = "T_ROLE_AUTH", joinColumns = @JoinColumn(name = "C_ROLE"), inverseJoinColumns = @JoinColumn(name = "C_AUTH"))
    private List<Authority> authorities;

}
