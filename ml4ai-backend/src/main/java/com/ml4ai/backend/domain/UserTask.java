package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import com.ml4ai.backend.domain.base.ProcessStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/12.
 */
@Setter
@Getter
@Entity
@Table(name = "T_USER_TASK")
public class UserTask extends BaseAuditEntity {

    @ManyToOne
    @JoinColumn(name = "c_user")
    private User user;

    @ManyToOne
    @JoinColumn(name = "c_task")
    private Task task;

    @Column
    private ProcessStatus processStatus;
}
