package com.ml4ai.backend.domain.base;

import lombok.Getter;

/**
 * Created by uesr on 2018/9/9.
 */
@Getter
public enum ProcessStatus {

    New("新建"),
    WaitTake("待接单"),
    Process("办理中"),
    Complete("完成");

    private String name;

    private ProcessStatus(String name) {
        this.name = name;
    }

}
