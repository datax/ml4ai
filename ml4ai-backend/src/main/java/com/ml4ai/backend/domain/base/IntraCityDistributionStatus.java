package com.ml4ai.backend.domain.base;

import lombok.Getter;

/**
 * Created by uesr on 2018/9/13.
 */
@Getter
public enum IntraCityDistributionStatus {

    NEW("新建"),
    CONFIRM("确认"),
    WAIT_TAKE("待接订单"),
    TRANSPORT("运输中"),
    ARRIVED("已送达"),
    BACK("取消");

    private String name;

    IntraCityDistributionStatus(String name) {
        this.name = name;
    }

}
