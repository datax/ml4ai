package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.IntraCityDistributionStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by uesr on 2018/9/10.
 */
@Getter
@Setter
@Entity
@Table(name = "T_INTRA_CITY_DISTRIBUTION_TASK")
public class IntraCityDistributionTask extends Task {

    @Column
    private Double sourceX;

    @Column
    private Double sourceY;

    @Column(name = "sourceAddressInfo")
    private String sourceAddressInfo;

    @Column
    private String sourceAddress;

    @Column
    private String sourceLink;

    @Column
    private String sourceTelephone;

    @Column
    private Double destinationX;

    @Column
    private Double destinationY;

    @Column(name = "destinationAddressInfo")
    private String destinationAddressInfo;

    @Column
    private String destinationAddress;

    @Column
    private String destinationLink;

    @Column
    private String destinationTelephone;

    @Column
    private String commodityName;

    @Enumerated(EnumType.STRING)
    @Column
    private IntraCityDistributionStatus intraCityDistributionStatus;

    @Column
    private Double distance;
}
