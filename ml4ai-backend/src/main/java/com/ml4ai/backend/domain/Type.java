package com.ml4ai.backend.domain;

import com.ml4ai.backend.domain.base.BaseAuditEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by uesr on 2018/9/12.
 */
@Data
@Entity
@Table(name = "T_TYPE")
public class Type extends BaseAuditEntity {

    @Column
    private String typeName;

    @Column
    private String typeCode;

    @Column
    private Type parent;

}
