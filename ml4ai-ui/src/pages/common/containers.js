import React from 'react';
import { MenuComponent } from '../management/menu';

//头部
class XHeader extends React.Component {
    render() {
        return (
            <div {...(this.props.properties ? this.properties : {})} style={{ ...(this.props.style ? this.props.style : {}), position: "relative", display: 'block', height: '100px', width: '100%' }}>
                {this.props.children}
            </div>
        );
    }
}

//中间
class AppContainer extends React.Component {
    render() {
        return (
            <div {...(this.props.properties ? this.props.properties : {})} style={{ ...(this.props.style ? this.props.style : {}), display: 'block', position: 'absolute', margin: '0 0 0 0', top: "100px", bottom: "0px", left: "0px", right: "0px", height: "auto", width: '100%' }}>
                {this.props.children}
            </div >
        );
    }
}

export { XHeader, AppContainer };
